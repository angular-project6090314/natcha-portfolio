import { Component } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  isDropdownVisible = false;

  openDropdown(): void {
    if(this.isDropdownVisible)
      this.isDropdownVisible = false;
    else
      this.isDropdownVisible = true;
  }
}
