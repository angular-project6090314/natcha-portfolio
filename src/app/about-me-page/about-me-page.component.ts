import { Component } from '@angular/core';

@Component({
  selector: 'app-about-me-page',
  templateUrl: './about-me-page.component.html',
  styleUrls: ['./about-me-page.component.css']
})
export class AboutMePageComponent {
  skills = [
    {
      id:1,
      name: 'Web Application (Full Stack)',
      detail: 'Vue.js, Angular, Node.js, SQL, MongoDB, RESTful API'
    },
    {
      id:2,
      name: 'Mobile Application',
      detail: 'Flutter, Kotlin'
    },
    {
      id:3,
      name: 'UX / UI Design & Responsive Template',
      detail: 'Figma, HTML / CSS, JavaScript, Bootstrap'
    },
    {
      id:4,
      name: 'Others',
      detail: 'JAVA, COBOL'
    }
  ]
}
