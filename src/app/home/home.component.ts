import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../services/project/project.service';
import { Project } from '../shared/models/Project';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{

  projects: Project[] = [];

  constructor(private projectService: ProjectService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.projects = this.projectService.getAll();
    })
  }

  tagClass(tag: string) {
    var classList = '';
    if(tag == 'Team Project') {
      classList = 'bg-gradient';
    }else if(tag == 'Personal Project') {
      classList = 'bg-gradient2';
    }
    return classList;
  }
}
