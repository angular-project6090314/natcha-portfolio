import { Component, OnInit, Output } from '@angular/core';
import { ProjectService } from '../services/project/project.service';
import { Project } from '../shared/models/Project';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-project-page',
  templateUrl: './project-page.component.html',
  styleUrls: ['./project-page.component.css']
})
export class ProjectPageComponent implements OnInit{
  project!: Project;
  imageUrl!: string;
  projectImageSeleted = 0;
  

  constructor(private projectService: ProjectService , private route: ActivatedRoute){
    route.params.subscribe((params) => {
      if(params['id'])
        this.project = projectService.getProjectById(params['id']);
    })
  }

  ngOnInit(): void {
    this.imageUrl = this.project.imageUrl[this.projectImageSeleted];
  }

  next(): void {
    if(this.projectImageSeleted >= this.project.imageUrl.length - 1){
      this.projectImageSeleted = 0;    
    }else {
       this.projectImageSeleted+=1;
    } 
    this.imageUrl = this.project.imageUrl[this.projectImageSeleted]; 
  }

  back(): void {
    if(this.projectImageSeleted > 0)
      this.projectImageSeleted-=1;
    this.imageUrl = this.project.imageUrl[this.projectImageSeleted];
  }

  smallImageClicked(imageUrl: string) {
    this.imageUrl = imageUrl;
    console.log('small', imageUrl)
  }

  tagClass(tag: string) {
    var classList = '';
    if(tag == 'Team Project') {
      classList = 'bg-gradient';
    }else if(tag == 'Personal Project') {
      classList = 'bg-gradient2';
    }
    return classList;
  }
}
