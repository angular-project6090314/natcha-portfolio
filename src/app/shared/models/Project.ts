export class Project {
  id!: number;
  name!: string;
  desc!: string;
  detail!: string;
  tags?: string[];
  imageUrl!: string[];
}