import { Injectable } from '@angular/core';
import { Project } from 'src/app/shared/models/Project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor() { }

  getProjectById(id: number): Project {
    return this.getAll().find(project => project.id == id)!;
  }

  getAllProjectsBySearchTerm(searchTerm: string): Project[] {
    return this.getAll().filter(project =>
      project.name.toLowerCase().includes(searchTerm.toLowerCase()));
  }

  getAll(): Project[] {
    return [
      {
        id:1,
        name: 'Food Store Angular Project',
        desc: 'โปรเจกต์ร้านอาหาร สำหรับซื้อขายอาหารผ่านช่องทางออนไลน์',
        detail: 'Food Store คือโปรเจกต์ฝึกการใช้งาน Angular Framework เรียนรู้ด้วยตนเองครั้งแรก โดยภายในเว็บไซต์ Food Store จะแสดงเมนูอาหาร สามารถค้นหา หรือกดที่แต่ละเมนูเพื่อดูรายละเอียดได้ สามารถเพิ่มอาหารที่ต้องการซื้อลงตะกร้า ระบบจะคำนวณจำนวนและราคาอาหารทั้งหมดภายในตะกร้า สำหรับดำเนินการซื้อในขั้นต่อไป',
        tags: ['Angular', 'Frontend', 'Personal Project'],
        imageUrl: [
          '/assets/images/projects/project1/project1-1.png',
          '/assets/images/projects/project1/project1-2.png',
          '/assets/images/projects/project1/project1-3.png',
          '/assets/images/projects/project1/project1-4.png',
          '/assets/images/projects/project1/project1-5.png'
        ]
      },
      {
        id:2,
        name: 'Accommodation and Hotel Review Article Website',
        desc: 'ออกแบบและสร้าง Template เว็บไซต์สำหรับอ่านบทความรีวิวที่พักและโรงแรมต่าง ๆ',
        detail: 'โปรเจกต์นี้เป็นโปรเจกต์สหกิจ ณ บริษัท เทนฟิวส์ จำกัด ออกแบบและสร้าง Responsive Template โดยใช้ HTML/CSS, JavaScript และ Bootstrap รองรับอุปกรณ์ Mobile และ Tablet โปรเจกต์นี้มีหน้าจอรวมทั้งหมด 18 หน้าจอด้วยกัน ในที่นี้จะแสดงภาพตัวอย่างหน้าจอเพียงบางส่วน',
        tags: ['UX/UI Design', 'Personal Project'],
        imageUrl: [
          '/assets/images/projects/project2/project2-1.png',
          '/assets/images/projects/project2/project2-2.png'
        ]
      },
      {
        id:3,
        name: 'Room Reservation Vue.js Project',
        desc: 'โปรเจกต์จองห้องและสถานที่ภายในมหาวิทยาลัยบูรพา สำหรับบุคลากรภายในมหาวิทยาลัย',
        detail: 'โปรเจกต์รายวิชา Web Application ของอาจารย์วรวิทย์ โดยทำหน้าที่ Full Stack ใช้ Vue.js เป็น Frontend Framework และเขียนฝั่ง Server โดยใช้ MongoDB เป็นฐานข้อมูล โปรแกรมนี้จะเป็นโปรแกรมสำหรับจองห้อง/สถานที่ภายในมหาวิทยาลัยบูรพา โดยสามารถเข้าสู่ระบบได้ทั้ง นิสิต อาจารย์ แอดมินหน่วยงาน และแอดมินระบบ ซึ่งผู้ใช้งานแต่ละสถานะจะมีฟังก์ชันที่สามารถใช้งานได้แตกต่างกันไป',
        tags: ['Vue.js', 'Full Stack', 'Team Project'],
        imageUrl: [
          '/assets/images/projects/project3/project3-1.png',
          '/assets/images/projects/project3/project3-2.png'
        ]
      },
      {
        id:4,
        name: 'Book Store Flutter Project',
        desc: 'โปรเจกต์ Mobile Application ร้านหนังสือ สร้าง UI โดยใช้ Flutter',
        detail: 'โปรเจกต์รายวิชา Mobile Development I ของอาจารย์จักรินทร์ เขียน Application ร้านหนังสือโดยใช้ภาษา Dart และ Flutter ในการพัฒนา ภายใน App สามารถเลือกดูหนังสือและกดดูหน้าเมนูต่าง ๆ ได้',
        tags: ['Flutter', 'Frontend', 'Personal Project'],
        imageUrl: [
          '/assets/images/projects/project4/project4-1.png'
        ]
      },
      // {
      //   id:5,
      //   name: 'Coffee Shop JAVA Project',
      //   desc: 'โปรเจกต์ร้านอาหาร สำหรับซื้อขายอาหารผ่านช่องทางออนไลน์',
      //   detail: '',
      //   tags: ['JAVA', 'Full Stack', 'Team Project'],
      //   imageUrl: ['/assets/images/projects/project-1.jpg']
      // }
    ]
  }
}
