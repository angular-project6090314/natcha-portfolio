import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../services/project/project.service';
import { Project } from '../shared/models/Project';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-projects-page',
  templateUrl: './projects-page.component.html',
  styleUrls: ['./projects-page.component.css']
})
export class ProjectsPageComponent implements OnInit{
  projects: Project[] = [];
  constructor(private projectService: ProjectService, private route: ActivatedRoute){}
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if(params['searchTerm'])
        this.projects = this.projectService.getAllProjectsBySearchTerm(params['searchTerm']);
      else
        this.projects = this.projectService.getAll();
    })
  }

  tagClass(tag: string) {
    var classList = '';
    if(tag == 'Team Project') {
      classList = 'bg-gradient';
    }else if(tag == 'Personal Project') {
      classList = 'bg-gradient2';
    }
    return classList;
  }
}
